terraform {
  required_providers {
    docker = {
      source  = "kreuzwerker/docker"
      version = "~> 2.22.0"
    }
  }
}

module "my_container" {
        source = "./my_modules/container_maker"                 # path to our child module "container_maker"
        container_name = var.container_name_from_root           # this value provided in the root will override the value for var.container_name within the child module
}
