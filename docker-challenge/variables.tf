variable "container_name" {
      description = "Value of the name for the Docker container"
       type        = string
       default    = "AltaReseatchwebService"
     }

    variable "internal_port" {
         description = "internal port to be used"
         type             = number
        default          = 9876
    }
  variable "external_port" {
         description = "external port to be used"
         type             = number
        default          = 5432
    }
